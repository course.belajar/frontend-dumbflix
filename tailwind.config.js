/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        base: "#1F1F1F",
        "red-base": "#E50914",
        "red-secondary": "#FF0742",
        "gray-base": "#D2D2D2",
        "gray-secondary": "#929292",
        "green-base": "#0ACF83",
        "orange-base": "#F7941E",
        "blue-base": "#1C9CD2",
      },
      boxShadow: {
        profile: "0px 0px 4px rgba(5, 5, 5, 0.08);",
      },
    },
  },
  plugins: [],
};
